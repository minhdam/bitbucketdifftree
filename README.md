# README #

### What is this repository for? ###

* Create a chrome extension that allow showing diff as tree structure in a PR of bitbucket.
* Version: 1.0.0

### How to set up? ###

* Open Chrome and access chrome extensions by **chrome://extensions/**
* Make sure the **Developer mode** checkbox is checked
* Click **Loan unpacked extension...** and point to the **Source** folder of this repo
* Finish! You will see an "B" icon added to the extension areas

### How to run the extension ###

* Open a bitbucket pull request
* Click on "B" icon to activate the diff tree extension
* Enjoy!


